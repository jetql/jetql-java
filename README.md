# com.jetql.JetQL

A GraphQL counterpart writing queries as javascript objects and json.


### Grammar

Terminal | Meaning
---:|:---
```SHAPE``` | ```shape```
```TYPE``` | ```type```
```ROOT``` | ```root```
```EXTEND``` | ```extends```
```BLOCK_BEGIN``` | ```{```
```BLOCK_END``` | ```}```
```CALLABLE_BEGIN``` | ```(```
```CALLABLE_END``` | ```)```
```NAME``` | ```[a-zA-Z][a-zA-Z0-9_]*```
```COLLECTION``` | ```[]```

rule | expansion 
---:|:---
```definition``` | ```shape```
```definition``` | ```type```
```definition``` | ```root```
```shape``` | ```SHAPE NAME block```
```type``` | ```TYPE NAME SPACE extend_block```
```root``` | ```ROitOT block```
```extend_block``` | ```EXTEND NAME block```
```extend_block``` | ```block```
```block``` | ```BLOCK_BEGIN block_end```
```block_end``` | ```name_collection name_function block_end```
```block_end``` | ```BLOCK_END```
```name_collection``` | ```NAME COLLECTION```
```name_collection``` | ```NAME```
```name_function``` | ```NAME function```
```name_function``` | ```NAME```
```function``` | ```CALLABLE_BEGIN function_end```
```function_end``` | ```name_collection function_end```
```function_end``` | ```CALLABLE_END```

# GitLab

[![Build status](https://gitlab.com/jetql/jetql-java/badges/master/build.svg)](https://gitlab.com/jetql/jetql-java/commits/master)

## Canonical source

The canonical source of secata is [hosted on GitLab.com](https://gitlab.com/jetql/jetql-java).
