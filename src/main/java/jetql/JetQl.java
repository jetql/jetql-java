package jetql;

import jetql.parser.model.JetQlSchema;
import java.util.Map;

public class JetQl {

  private JetQlSchema schema;
  private Map<String, Class> nameToClass;

  JetQl(JetQlSchema schema, Map<String, Class> nameToClass) {
    this.schema = schema;
    this.nameToClass = nameToClass;
  }

  public JetQlResult query(JetQlQuery query) {
    //todo process
    return JetQlResult.create();
  }

  public JetQlResult update(JetQlUpdate update) {
    //todo process
    return JetQlResult.create();
  }
}