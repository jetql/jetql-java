package jetql.parser;

import jetql.parser.tokenizing.JetQlToken;

public class JetQlParserException extends RuntimeException {

  private static final long serialVersionUID = -1009747984332258423L;

  private JetQlToken token = null;

  public JetQlParserException(String message) {
    super(message);
  }

  public JetQlParserException(String message, JetQlToken token) {
    super(message);
    this.token = token;
  }

  public JetQlToken getToken() {
    return token;
  }

  public String getMessage() {
    String msg = super.getMessage();
    if (token != null) {
      msg = msg.replace("%s", token.getSequence());
    }
    return msg;
  }
}
