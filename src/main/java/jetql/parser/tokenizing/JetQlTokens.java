package jetql.parser.tokenizing;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public enum JetQlTokens {
  SHAPE("shape"),
  TYPE("type"),
  ROOT("root"),
  EXTEND("\\<"),
  BLOCK_BEGIN("\\{"),
  BLOCK_END("\\}"),
  CALLABLE_BEGIN("\\("),
  CALLABLE_END("\\)"),
  COMMA(","),
  NAME("[a-zA-Z][a-zA-Z0-9_]*"),
  COLLECTION("\\[\\]"),
  IDENTIFIER("!"),
  EOF(null);

  private String regex;
  private Pattern pattern;

  JetQlTokens(String pattern) {
    this.regex = pattern;
  }

  public Matcher matcher(String source) {
    if (regex == null) {
      return null;
    }
    if (pattern == null) {
      pattern = Pattern.compile("^(" + regex + ")");
    }
    return pattern.matcher(source);
  }

  public JetQlToken create(String sequence) {
    return new JetQlToken(this, sequence);
  }
}
