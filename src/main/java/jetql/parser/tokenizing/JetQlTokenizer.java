package jetql.parser.tokenizing;

import jetql.parser.JetQlParserException;
import java.util.LinkedList;
import java.util.regex.Matcher;

public class JetQlTokenizer {

  private LinkedList<JetQlTokens> tokens;

  public JetQlTokenizer() {
    tokens = new LinkedList<>();
  }

  public void add(JetQlTokens token) {
    tokens.add(token);
  }

  public LinkedList<JetQlToken> tokenize(String source) {
    LinkedList<JetQlToken> tokenList = new LinkedList<>();
    String temp = source.trim();
    while (!temp.equals("")) {
      boolean match = false;
      for (JetQlTokens tokens : this.tokens) {
        Matcher matcher = tokens.matcher(temp);
        if (matcher != null && matcher.find()) {
          match = true;

          String tokenString = matcher.group().trim();
          tokenList.add(tokens.create(tokenString));

          temp = matcher.replaceFirst("").trim();
          break;
        }
      }
      if (!match) {
        throw new JetQlParserException("Unexpected character in input: " + temp);
      }
    }
    return tokenList;
  }
}
