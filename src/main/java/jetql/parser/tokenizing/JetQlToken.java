package jetql.parser.tokenizing;

public class JetQlToken {
  private final JetQlTokens type;
  private final String sequence;

  JetQlToken(JetQlTokens type, String sequence) {
    this.type = type;
    this.sequence = sequence;
  }

  public boolean is(JetQlTokens type) {
    return this.type.equals(type);
  }

  public String getSequence() {
    return sequence;
  }

  @Override
  public String toString() {
    return "Token{" +
        "type=" + type +
        ", sequence='" + sequence + '\'' +
        '}';
  }
}