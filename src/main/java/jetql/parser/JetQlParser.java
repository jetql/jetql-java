package jetql.parser;

import jetql.parser.model.JetQlDefinition;
import jetql.parser.model.JetQlField;
import jetql.parser.model.JetQlFunction;
import jetql.parser.model.JetQlMember.MemberType;
import jetql.parser.model.JetQlRoot;
import jetql.parser.model.JetQlSchema;
import jetql.parser.model.JetQlShape;
import jetql.parser.model.JetQlType;
import jetql.parser.tokenizing.JetQlToken;
import jetql.parser.tokenizing.JetQlTokenizer;
import jetql.parser.tokenizing.JetQlTokens;
import java.util.LinkedList;

public class JetQlParser {

  private static final JetQlTokenizer tokenizer;

  static {
    tokenizer = new JetQlTokenizer();
    for (JetQlTokens tokens : JetQlTokens.values()) {
      if (tokens.matcher("") != null) {
        tokenizer.add(tokens);
      }
    }
  }

  private LinkedList<JetQlToken> tokens;
  private JetQlToken lookahead;
  private JetQlSchema schema;

  private JetQlDefinition currentDefinition;
  private JetQlFunction currentFunction;
  private MemberType currentType;
  private String currentName;

  /**
   * Parses string to a jetql-java schema.
   *
   * @param source as string
   * @return a JetQlSchema
   */
  public JetQlSchema parse(String source) {
    schema = new JetQlSchema();
    tokens = tokenizer.tokenize(source);
    lookahead = tokens.getFirst();

    while (!lookahead.is(JetQlTokens.EOF)) {
      definition();
    }

    return schema;
  }

  private void nextToken() {
    tokens.removeFirst();
    if (tokens.isEmpty()) {
      lookahead = JetQlTokens.EOF.create("");
    } else {
      lookahead = tokens.getFirst();
    }
  }

  private void definition() {
    if (lookahead.is(JetQlTokens.SHAPE)) {
      System.out.println("definition -> shape");
      shape();
    } else if (lookahead.is(JetQlTokens.TYPE)) {
      System.out.println("definition -> type");
      type();
    } else if (lookahead.is(JetQlTokens.ROOT)) {
      System.out.println("definition -> root");
      root();
    } else {
      throw new JetQlParserException("Expected definition at: " + lookahead);
    }
  }

  private void shape() {
    nextToken();
    System.out.println("shape -> SHAPE NAME block");
    currentDefinition = new JetQlShape(lookahead.getSequence());
    schema.addShape((JetQlShape) currentDefinition);
    nextToken();
    block();
  }

  private void type() {
    nextToken();
    System.out.println("type -> TYPE NAME SPACE extend_block");
    currentDefinition = new JetQlType(lookahead.getSequence());
    schema.addType((JetQlType) currentDefinition);
    nextToken();
    extend_block();
  }

  private void root() {
    nextToken();
    System.out.println("root -> ROOT block");
    currentDefinition = new JetQlRoot();
    schema.setRoot((JetQlRoot) currentDefinition);
    block();
  }

  private void extend_block() {
    if (lookahead.is(JetQlTokens.EXTEND)) {
      System.out.println("extend_block -> EXTEND NAME block");
      nextToken();
      ((JetQlType) currentDefinition).setParent(schema.getShape(lookahead.getSequence()));
      nextToken();
      block();
    } else {
      System.out.println("extend_block -> block");
      block();
    }
  }

  private void block() {
    System.out.println("block -> BLOCK_BEGIN block_end");
    nextToken();
    block_end();
  }

  private void block_end() {
    if (!lookahead.is(JetQlTokens.BLOCK_END)) {
      System.out.println("block_end -> member block_end");
      name_collection();
      name_function();
      block_end();
    } else {
      System.out.println("block_end -> BLOCK_END");
      nextToken();
    }
  }

  private void name_collection() {
    String name = lookahead.getSequence();
    currentType = new MemberType(name);
    nextToken();
    if (lookahead.is(JetQlTokens.COLLECTION)) {
      System.out.println("name_collection -> NAME COLLECTION");
      currentType.setCollection(true);
      nextToken();
    } else {
      System.out.println("name_collection -> NAME");
    }
  }

  private void name_function() {
    currentName = lookahead.getSequence();
    nextToken();
    if (lookahead.is(JetQlTokens.CALLABLE_BEGIN)) {
      System.out.println("member -> NAME function");
      function();
    } else {
      System.out.println("member -> NAME");
      currentDefinition.addField(new JetQlField(currentType, currentName));
    }
  }

  private void function() {
    System.out.println("function -> CALLABLE_BEGIN function_end");
    nextToken();
    currentFunction = new JetQlFunction(currentType, currentName);
    currentDefinition.addFunction(currentFunction);
    function_end();
  }

  private void function_end() {
    if (!lookahead.is(JetQlTokens.CALLABLE_END)) {
      System.out.println("function_end -> argument function_end");
      name_collection();
      currentFunction.addArgument(currentType);
      function_end();
    } else {
      System.out.println("function_end -> CALLABLE_END");
      nextToken();
    }
  }
}
