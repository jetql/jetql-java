package jetql.parser.model;

public abstract class JetQlMember {

  private MemberType type;
  private String name;

  JetQlMember(MemberType type, String name) {
    this.type = type;
    this.name = name;
  }

  public MemberType getType() {
    return type;
  }

  public String getName() {
    return name;
  }

  public static class MemberType {

    private String name;
    private boolean collection;

    public MemberType(String name) {
      this.name = name;
      this.collection = false;
    }

    public String getName() {
      return name;
    }

    public void setCollection(boolean collection) {
      this.collection = collection;
    }

    public boolean isCollection() {
      return collection;
    }
  }
}
