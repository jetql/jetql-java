package jetql.parser.model;

import jetql.parser.JetQlParserException;
import java.util.LinkedList;

public class JetQlSchema {

  private JetQlRoot root;
  private LinkedList<JetQlShape> shapes;
  private LinkedList<JetQlType> types;

  public JetQlSchema() {
    this.shapes = new LinkedList<>();
    this.types = new LinkedList<>();
  }

  public JetQlRoot getRoot() {
    return root;
  }

  public void setRoot(JetQlRoot root) {
    if (this.root != null) {
      throw new JetQlParserException("Root already set");
    }
    this.root = root;
  }

  public void addShape(JetQlShape shape) {
    this.shapes.add(shape);
  }

  public LinkedList<JetQlShape> getShapes() {
    return shapes;
  }

  public void addType(JetQlType type) {
    this.types.add(type);
  }

  public LinkedList<JetQlType> getTypes() {
    return types;
  }

  public JetQlShape getShape(String sequence) {
    for (JetQlShape shape : shapes) {
      if (sequence.equals(shape.getName())) {
        return shape;
      }
    }
    for (JetQlType type : types) {
      if (sequence.equals(type.getName())) {
        return type;
      }
    }
    throw new JetQlParserException("shape or type not found: " + sequence);
  }
}
