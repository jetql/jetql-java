package jetql.parser.model;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class JetQlFunction extends JetQlMember {

  private LinkedList<MemberType> arguments;

  public JetQlFunction(MemberType type, String name) {
    super(type, name);
    this.arguments = new LinkedList<>();
  }

  public void addArgument(MemberType argument){
    this.arguments.add(argument);
  }

  public List<MemberType> getArguments() {
    return Collections.unmodifiableList(arguments);
  }
}
