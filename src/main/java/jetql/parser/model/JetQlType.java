package jetql.parser.model;

import jetql.parser.JetQlParserException;

public class JetQlType extends JetQlShape {

  private JetQlShape parent;

  public JetQlType(String name) {
    super(name);
  }

  public JetQlShape getParent() {
    return parent;
  }

  public void setParent(JetQlShape parent) {
    if (this.parent != null) {
      throw new JetQlParserException("Parent already set");
    }
    this.parent = parent;
  }
}
