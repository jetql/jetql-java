package jetql.parser.model;

public class JetQlShape extends JetQlDefinition {

  private String name;

  public JetQlShape(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
