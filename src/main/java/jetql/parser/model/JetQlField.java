package jetql.parser.model;

public class JetQlField extends JetQlMember {

  public JetQlField(MemberType type, String name) {
    super(type, name);
  }
}
