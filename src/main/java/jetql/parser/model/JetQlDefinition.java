package jetql.parser.model;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public abstract class JetQlDefinition {

  private LinkedList<JetQlField> fields;
  private LinkedList<JetQlFunction> functions;

  JetQlDefinition() {
    this.fields = new LinkedList<>();
    this.functions = new LinkedList<>();
  }

  public void addField(JetQlField field){
    this.fields.add(field);
  }

  public List<JetQlField> getFields() {
    return Collections.unmodifiableList(fields);
  }

  public void addFunction(JetQlFunction function){
    this.functions.add(function);
  }

  public List<JetQlFunction> getFunctions() {
    return Collections.unmodifiableList(functions);
  }

  public List<JetQlMember> members(){
    LinkedList<JetQlMember> result = new LinkedList<>();
    result.addAll(fields);
    result.addAll(functions);
    return Collections.unmodifiableList(result);
  }
}
