package jetql;

import java.io.Serializable;
import java.util.HashMap;

public class JetQlResult implements Serializable {

  private HashMap<String, Object> results;

  JetQlResult() {
    this.results = new HashMap<>();
  }

  public static JetQlResult create() {
    return new JetQlResult();
  }

  public JetQlResult result(String name, Object value) {
    this.results.put(name, value);
    return this;
  }

  public HashMap<String, Object> getResults() {
    return results;
  }
}
