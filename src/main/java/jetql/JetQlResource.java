package jetql;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

public abstract class JetQlResource {

  private JetQl jetQl;

  public JetQlResource(JetQl jetQl) {
    this.jetQl = jetQl;
  }

  @POST
  @Path("/call")
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  public JetQlResult update(JetQlUpdate input) {
    return jetQl.update(input);
  }

  @POST
  @Path("/query")
  @Produces(MediaType.APPLICATION_JSON)
  @Consumes(MediaType.APPLICATION_JSON)
  public JetQlResult query(JetQlQuery input) {
    return jetQl.query(input);
  }
}
