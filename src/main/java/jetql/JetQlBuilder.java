package jetql;

import java.util.Map;
import java.util.TreeMap;
import jetql.parser.JetQlParser;
import jetql.parser.model.JetQlSchema;

public class JetQlBuilder {

  private JetQlSchema schema;
  private Map<String, Class> nameToClass;

  private JetQlBuilder() {
    nameToClass = new TreeMap<>();
  }

  public static JetQlBuilder fromString(String schemaAsString) {
    JetQlParser parser = new JetQlParser();
    JetQlSchema schema = parser.parse(schemaAsString);
    JetQlBuilder builder = new JetQlBuilder();
    builder.schema = schema;
    return builder;
  }

  public JetQlBuilder register(Class... classes) {
    for (Class clazz : classes) {
      this.nameToClass.put(clazz.getSimpleName(), clazz);
    }
    return this;
  }

  public JetQl build() {
    return new JetQl(schema, nameToClass);
  }
}
