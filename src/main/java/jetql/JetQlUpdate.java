package jetql;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class JetQlUpdate extends JetQlQuery {

  private HashMap<String, Object> values;

  JetQlUpdate() {
    super();
    this.values = new HashMap<>();
  }

  public static JetQlUpdate create(){
    return new JetQlUpdate();
  }

  @Override
  public JetQlUpdate query(String query) {
    return (JetQlUpdate) super.query(query);
  }

  @Override
  public JetQlUpdate parameter(String name, Object value) {
    return (JetQlUpdate) super.parameter(name, value);
  }

  public JetQlUpdate value(String name, Object value) {
    this.values.put(name, value);
    return this;
  }

  public Map<String, Object> getValues() {
    return Collections.unmodifiableMap(values);
  }
}
