package jetql;

import java.io.Serializable;
import java.util.HashMap;

public class JetQlQuery implements Serializable {

  private String query;
  private HashMap<String, Object> parameters;

  JetQlQuery() {
    parameters = new HashMap<>();
  }

  public static JetQlQuery create() {
    return new JetQlQuery();
  }

  public JetQlQuery query(String query){
    this.query = query;
    return this;
  }

  public JetQlQuery parameter(String name, Object value) {
    this.parameters.put(name, value);
    return this;
  }

  public String getQuery() {
    return query;
  }

  public HashMap<String, Object> getParameters() {
    return parameters;
  }
}
