package example;

import jetql.JetQlBuilder;
import jetql.JetQlResource;
import example.model.Inherited;
import example.model.Leaf;
import example.model.Node;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import javax.ws.rs.Path;

@Path("/jetql")
public class MainResource extends JetQlResource {

  public MainResource() {
    super(JetQlBuilder.fromString(schemaAsString())
        .register(
            Inherited.class,
            Leaf.class,
            Node.class
        )
        .build());
  }

  private static String schemaAsString() {
    try {
      return new String(
          MainResource.class.getResourceAsStream("schema.jetql").readAllBytes(),
          StandardCharsets.UTF_8);
    } catch (IOException e) {
      throw new RuntimeException("Could not read schema from file", e);
    }
  }
}
