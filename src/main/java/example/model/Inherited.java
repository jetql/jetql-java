package example.model;

import java.time.Instant;
import java.util.UUID;

public abstract class Inherited {
  private UUID id;
  private Instant createTs;
  private Instant modifyTs;
  private Instant deleteTs;
  private String name;

  UUID getId() {
    return id;
  }
}