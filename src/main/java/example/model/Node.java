package example.model;

import java.util.List;
import java.util.UUID;

public class Node extends Inherited {

  private Node parent;
  private List<Inherited> children;

  public Node getParent() {
    return parent;
  }

  public void setParent(Node parent) {
    this.parent = parent;
  }

  public List<Inherited> getChildren() {
    return children;
  }

  public void setChildren(List<Inherited> children) {
    this.children = children;
  }

  public Boolean addChild(Inherited child) {
    if (children.contains(child)) {
      return false;
    }
    children.add(child);
    return true;
  }

  public Inherited getChild(UUID uuid) {
    for (Inherited child : children) {
      if (uuid.equals(child.getId())) {
        return child;
      }
    }
    return null;
  }
}