package example;

import java.net.URI;
import javax.ws.rs.core.UriBuilder;
import org.eclipse.jetty.server.Server;
import org.glassfish.jersey.jetty.JettyHttpContainerFactory;
import org.glassfish.jersey.server.ResourceConfig;


public class Main {

  public static void main(String[] args) {
    ResourceConfig resourceConfig = new ResourceConfig(MainResource.class);
    URI baseUri = UriBuilder.fromUri("http://localhost").port(1337).build();
    Server jettyServer = JettyHttpContainerFactory.createServer(baseUri, resourceConfig, false);
    try {
      try {
        jettyServer.start();
        jettyServer.join();
      } finally {
        if (jettyServer.isRunning()) {
          jettyServer.stop();
        }
        jettyServer.destroy();
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}