package jetql.parser;

import static org.assertj.core.api.Assertions.assertThat;

import jetql.parser.model.JetQlSchema;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import org.junit.Test;

public class JetQlParserTest {

  @Test
  public void parse() throws IOException {
    String source = new String(this.getClass().getResourceAsStream("schema.jetql").readAllBytes(), StandardCharsets.UTF_8);
    JetQlParser parser = new JetQlParser();
    JetQlSchema schema = parser.parse(source);
    assertThat(schema).isNotNull();
  }
}
